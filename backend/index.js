const express = require('express');
const news = require('./app/News');
const app = express();
const port = 8000;

app.use("/news", news);

app.listen(port, () => {
  console.log(`Server started at http://localhost:${port}`);
});