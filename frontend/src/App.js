import React from 'react';
import {Switch, Route, Link} from "react-router-dom";
import News from './containers/News/News';
import NewsAddForm from "./containers/NewsAddForm/NewsAddForm";
import Comments from "./containers/Comments/Comments";
import './App.css';

function App() {
  return (
    <>
      <div className="Toolbar">
        <Link to="/news" className="logo">News</Link>
      </div>
      <Switch>
        <Route path="/news" exact component={News}/>
        <Route path="/addNews" component={NewsAddForm}/>
        <Route path="/comments" component={Comments}/>
      </Switch>
    </>
  );
}

export default App;
