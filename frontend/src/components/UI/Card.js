import React from 'react';

const Card = props => {
  return (
      <div>
        <h3>{props.title}</h3>
        <p>{props.text}</p>
        <button type="button">ADD</button>
      </div>
  );
};

export default Card;