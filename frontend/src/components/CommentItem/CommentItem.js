import React from 'react';

const CommentItem = props => {
  return (
      <div className="comment-item">
        <p>{props.text}</p>
        <button>Delete</button>
      </div>
  );
};

export default CommentItem;