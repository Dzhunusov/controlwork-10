import React from 'react';
import './NewsItem.css';

const NewsItem = (text, date) => {
  return (
      <div className="news-item">
        <div className="news-pic">X</div>
        <div className="news-info">
          <p>{text}</p>
          <p>{date}</p>
          <button>Read full news>></button>
        </div>
        <div className="news-item-btn">
          <button >Delete</button>
        </div>
      </div>
  );
};

export default NewsItem;