import React, {useEffect} from 'react';
import NewsItem from "../../components/NewsItem/NewsItem";
import './News.css';
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {fetchNews} from "../../store/actions/newsActions";

const News = () => {
  const dispatch = useDispatch();
  const news = useSelector(state => state.news.news);

  useEffect(() => {
    dispatch(fetchNews());
  }, [dispatch]);

  return (
      <div className="main-block">
        <div className="main-block-header">
          <h1>Posts</h1>
          <Link to="/addNews" className="btn">Add new post</Link>
        </div>
        <div>
          {news.map(post => {
            return <NewsItem
                text={post.text}
                date={post.dateTime}
            />
          })}
        </div>
      </div>
  );
};

export default News;