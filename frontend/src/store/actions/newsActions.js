import axiosApi from "../../axiosAPI";
import {CREATE_NEWS_SUCCESS, FETCH_NEWS_SUCCESS} from "../actionTypes";

const fetchNewsSuccess = news => {
  return {type: FETCH_NEWS_SUCCESS, news};
};

export const fetchNews = () => {
  return dispatch => {
    return axiosApi.get("/news").then(response => {
      dispatch(fetchNewsSuccess(response.data));
    });
  };
};

const createNewsSuccess = () => {
  return {type: CREATE_NEWS_SUCCESS};
};

export const createNews = newsData => {
  return dispatch => {
    return axiosApi.post("/news", newsData).then(() => {
      dispatch(createNewsSuccess());
    });
  };
};